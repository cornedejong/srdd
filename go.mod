module solrad.nl/sdds

go 1.17

require (
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/radovskyb/watcher v1.0.7
	github.com/segmentio/fasthash v1.0.3
)

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/mmmorris1975/go-upnp v0.0.0-20181119221030-904a803f1d17 // indirect
	github.com/r3labs/diff v1.1.0 // indirect
	github.com/r3labs/diff/v2 v2.15.0 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	google.golang.org/appengine v1.6.6 // indirect
)
