package sddp

import (
	"fmt"
	"hash/fnv"
	"net"
	"time"
)

type MessageHashMap struct {
	Messages []string
}

func (m *MessageHashMap) Hash(sender net.Addr, message string) int32 {
	base := sender.String() + message + fmt.Sprint(time.Now().Unix())

	h := fnv.New32a()
	h.Write([]byte(base))
	return int32(h.Sum32())
}

func (m *MessageHashMap) Add(hash string) {
	m.Messages = append(m.Messages, hash)
}

func (m *MessageHashMap) Contains(hashIn string) bool {
	for _, hash := range m.Messages {
		if hash == hashIn {
			return true
		}
	}
	return false
}
