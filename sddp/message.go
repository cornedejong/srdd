package sddp

import (
	"encoding/json"
	"fmt"
	"net"
	"time"

	uuid "github.com/nu7hatch/gouuid"
	"github.com/segmentio/fasthash/fnv1a"
)

type Spread struct {
	Count    int
	WaitTime time.Duration
}

type Message struct {
	Debug     bool
	Hash      string
	Sender    net.Addr
	Recipient net.Addr
	Message   string
	Data      interface{}
	Burst     int
	Spread    Spread
}

func NewMessageFromString(sender net.Addr, messageString string) (Message, error) {
	hashString, remainder := splitAtWordNumber(1, messageString)

	message, dataString := splitAtWordNumber(1, remainder)
	msg := Message{
		Hash:    hashString,
		Sender:  sender,
		Message: message,
	}

	if msg.Message[0:1] == "$" {
		msg.Debug = true
		msg.Message = trimFirstCharacter(msg.Message)
	}

	var data interface{}

	if dataString != "" {
		err := json.Unmarshal([]byte(dataString), &data)
		if err != nil {
			return msg, err
		}
		msg.Data = data
	}

	return msg, nil
}

func (m *Message) GetBurst() int {
	if m.Burst == 0 {
		return 5
	}

	return m.Burst
}

func (m *Message) GetSpread() Spread {
	if m.Spread == (Spread{}) {
		m.Spread = Spread{
			WaitTime: time.Millisecond * 350,
			Count:    3,
		}
	}

	return m.Spread
}

func (m *Message) GenerateHash() string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	return fmt.Sprint(fnv1a.HashString64(u.String() + RandStringBytes(128)))
}

func (m *Message) String() string {
	messageString := fmt.Sprint(m.Hash) + " "

	if m.Debug {
		messageString += "$"
	}

	messageString += m.Message

	if m.Data != nil {
		dataString, err := json.Marshal(m.Data)
		if err != nil {
			panic(err)
		}
		messageString += " " + string(dataString)
	}

	return messageString
}
