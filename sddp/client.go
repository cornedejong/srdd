package sddp

import (
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type Client struct {
	LocalIPAddress  net.IP
	LocalUDPAddress *net.UDPAddr

	BroadcastIP   string
	BroadcastPort int

	ResponsePort int
}

func NewClient() Client {
	/* Get our local udp address */
	local, err := net.ResolveUDPAddr("udp4", ":8167")
	if err != nil {
		panic(err)
	}

	localIp := GetOutboundIP()
	ipSlice := strings.Split(localIp.String(), ".")
	ipSlice = ipSlice[:len(ipSlice)-1]
	ipSlice = append(ipSlice, "255")
	brIP := strings.Join(ipSlice, ".")

	return Client{
		LocalIPAddress:  localIp,
		LocalUDPAddress: local,
		BroadcastIP:     brIP,
		BroadcastPort:   8166,
		ResponsePort:    8167,
	}
}

func (c *Client) getBroadcastIp(port int) string {
	return c.BroadcastIP + ":" + fmt.Sprint(port)
}

func (c *Client) getResponseAddress(port int) *net.UDPAddr {
	/* Get our local udp address */
	responseAddress, err := net.ResolveUDPAddr("udp4", ":"+fmt.Sprint(port))
	if err != nil {
		panic(err)
	}
	return responseAddress
}

func (c *Client) GetBroadcastAddress() net.Addr {
	address, err := net.ResolveUDPAddr("udp4", c.getBroadcastIp(c.BroadcastPort))
	if err != nil {
		panic(err)
	}
	return address
}

func (c *Client) Broadcast(message Message) int {
	remote, err := net.ResolveUDPAddr("udp4", message.Recipient.String())
	if err != nil {
		panic(err)
	}

	conn, err := net.DialUDP("udp4", c.getResponseAddress(c.ResponsePort), remote)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	message.Hash = message.GenerateHash()

	var nb int
	for i := 0; i < message.GetBurst(); i++ {
		nb, err = conn.Write([]byte(message.String()))
		if err != nil {
			panic(err)
		}
		/* todo: check if the number of bytes is same as original message */
	}

	/* todo: return bool */
	return nb
}

func (c *Client) Listen(port int, ListenFor time.Duration, OnMessage func(message Message)) {
	pc, err := net.ListenPacket("udp4", ":"+fmt.Sprint(port))
	if err != nil {
		panic(err)
	}
	defer pc.Close()

	messageMap := MessageHashMap{Messages: []string{}}

	pc.SetReadDeadline(time.Now().Add(ListenFor))

	for {

		/* TODO: Add time limit for listening */

		buf := make([]byte, 1024)
		n, addr, err := pc.ReadFrom(buf)
		if err != nil {
			if strings.HasSuffix(err.Error(), "i/o timeout") {
				break
			}

			log.Println("Error while reading from socket Buffer: " + err.Error())
		}

		messageString := string(buf[:n])

		/* Create a Messsage struct */
		message, err := NewMessageFromString(addr, messageString)

		if messageMap.Contains(message.Hash) {
			continue
		}

		messageMap.Add(message.Hash)

		go OnMessage(message)
	}

}
