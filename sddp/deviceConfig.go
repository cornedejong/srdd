package sddp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
)

type Services []Service

type DeviceConfig struct {
	Location string   `json:"Location"`
	Hostname string   `json:"hostname"`
	Device   Device   `json:"device"`
	Services Services `json:"services"`
}

type Device struct {
	Manufacturer    string  `json:"manufacturer"`
	Type            string  `json:"type"`
	Revision        float32 `json:"revision"`
	Serial          int     `json:"serial"`
	SoftwareVersion string  `json:"softwareVersion"`
}

type Service struct {
	Status         string  `json:"status"`
	Name           string  `json:"name"`
	ShortName      string  `json:"shortName"`
	Version        float32 `json:"version"`
	Type           string  `json:"type"`
	Port           int     `json:"port"`
	ConnectionType string  `json:"connectionType"`
	Protocol       string  `json:"protocol"`
}

type Query struct {
}

func (s *Service) IsReachable(ipAddress string) bool {
	ln, err := net.Listen(s.ConnectionType, ipAddress+":"+fmt.Sprint(s.Port))

	if err != nil {
		return true
	}

	defer ln.Close()

	return false
}

func (s *Services) Find(query interface{}) (Services, error) {
	return Services{}, nil
}

func NewConfigurationFromDirectory(path string) (DeviceConfig, error) {
	conf := DeviceConfig{}

	/* Check if the configuration directory exists */
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Fatalln("Config directory does not exist at path: " + path)
		return conf, err
	}

	/* Lets check if we have the device json description file */
	if _, err := os.Stat(path + string(os.PathSeparator) + "device.json"); os.IsNotExist(err) {
		log.Fatalln("Config directory does not contain device.json (Device Descriptor) at path: " + path)
		return conf, err
	}

	/*************************************************/
	/* Loading device description json */
	/*************************************************/

	/* Decode the json file */
	var device Device
	file, _ := os.Open(path + string(os.PathSeparator) + "device.json")
	defer file.Close()

	decode := json.NewDecoder(file)
	if err := decode.Decode(&device); err != nil {
		panic(err)
	}
	conf.Device = device

	/*************************************************/
	/* Loading all service files */
	/*************************************************/

	servicesPath := path + string(os.PathSeparator) + "services"
	if _, err := os.Stat(servicesPath); os.IsNotExist(err) {
		// log.Fatalln("Config directory does not contain services directory at path: " + path + string(os.PathSeparator) + "services")
		err = os.MkdirAll(servicesPath, os.ModePerm)
		if err != nil {
			log.Fatalln("Error while creating the service directory at path: " + servicesPath)
			return conf, err
		}
	}

	files, err := ioutil.ReadDir(servicesPath)
	if err != nil {
		log.Fatal("Error while reading services directory" + err.Error())
		return conf, err
	}

	conf.Services = Services{}

	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".json") {
			var service Service
			file, _ := os.Open(servicesPath + string(os.PathSeparator) + file.Name())
			defer file.Close()

			decode := json.NewDecoder(file)
			if err := decode.Decode(&service); err != nil {
				panic(err)
			}

			conf.Services = append(conf.Services, service)
		}
	}

	/*************************************************/
	/* Getting local IP Address for the service */
	/*************************************************/

	conf.Location = GetOutboundIP().String()
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	conf.Hostname = hostname

	/*************************************************/
	/* All done, lets return it! :D */
	/*************************************************/

	return conf, nil
}

func (c *DeviceConfig) GetBroadcastableDescription() DeviceConfig {
	output := *c
	services := []Service{}
	output.Services = c.GetAliveService(c.Location)

	for _, service := range c.Services {
		if service.IsReachable(c.Location) {
			service.Status = "up"
		} else {
			service.Status = "down"
		}

		services = append(services, service)
	}

	output.Services = services
	return output
}

func (c *DeviceConfig) GetAliveService(ipAddress string) []Service {
	output := []Service{}

	for _, service := range c.Services {
		if service.IsReachable(ipAddress) {
			output = append(output, service)
		}
	}

	return output
}
