package sddp

import (
	"log"
	"math/rand"
	"net"
	"net/http"
	"strings"
	"time"
)

func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func sleep(duration time.Duration, endSignal chan<- bool) {
	time.Sleep(duration)
	endSignal <- true
}

func trimFirstCharacter(s string) string {
	for i := range s {
		if i > 0 {
			return s[i:]
		}
	}
	return s[:0]
}

func splitAtWordNumber(number int, value string) (string, string) {
	// Loop over all indexes in the string.
	for i := range value {
		// If we encounter a space, reduce the count.
		if value[i] == ' ' {
			number -= 1
			// When no more words required, return a substring.
			if number == 0 {
				return value[0:i], strings.TrimLeft(value[i:], " ")
			}
		}
	}
	// Return the entire string.
	return value, ""
}

func NetworkUp() (ok bool) {
	_, err := http.Get("http://clients3.google.com")
	if err != nil {
		return false
	}
	return true
}

func RandStringBytes(n int) string {
	letterBytes := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+-=,./;'\\[]<>?:\"|{}§±`~"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
