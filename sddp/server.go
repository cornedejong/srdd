package sddp

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/radovskyb/watcher"
)

type Server struct {
	LocalIPAddress  net.IP
	LocalUDPAddress *net.UDPAddr

	BroadcastIP   string
	BroadcastPort int

	Config DeviceConfig
}

func NewServer(pathToConfig string) Server {

	conf, err := NewConfigurationFromDirectory(pathToConfig)
	if err != nil {
		panic(err)
	}

	/* Get our local udp address */
	local, err := net.ResolveUDPAddr("udp4", ":8167")
	if err != nil {
		panic(err)
	}

	if _, err := os.Stat(pathToConfig); os.IsNotExist(err) {
		log.Fatalln("Config file does not exist at path: " + pathToConfig)
	}

	/* conf.Services = append(conf.Services, Service{
		Name:           "Simple Device Discovery Service",
		ShortName:      "sdds",
		Version:        1.0,
		Type:           "discovery",
		Port:           8166,
		ConnectionType: "udp",
		Protocol:       "sddp",
	}) */

	localIp := GetOutboundIP()
	ipSlice := strings.Split(localIp.String(), ".")
	ipSlice = ipSlice[:len(ipSlice)-1]
	ipSlice = append(ipSlice, "255")
	brIP := strings.Join(ipSlice, ".")

	return Server{
		LocalIPAddress:  localIp,
		LocalUDPAddress: local,
		BroadcastIP:     brIP,
		BroadcastPort:   8166,
		Config:          conf,
	}
}

func (s *Server) WatchForDeviceChanges(pathToConfig string) {
	/* Add notifier */
	w := watcher.New()
	w.SetMaxEvents(1)

	go func() {
		for {
			select {
			case <-w.Event:
				// fmt.Println(event) // Print the event's info.
				log.Println("Reloading device configuration...")
				conf, err := NewConfigurationFromDirectory(pathToConfig)
				if err != nil {
					panic(err)
				}

				// changed := s.GetConfigChangesBetween(s.Config, conf)

				s.Config = conf

				/* If there was an interrupt lets ne nice to the network and send a byebye message */
				_ = s.Broadcast(Message{
					Recipient: s.GetBroadcastAddress(),
					Message:   "sddp:changed",
				})

				log.Println("<- Broadcasted: sddp:changed >> " + s.GetBroadcastAddress().String())
			case err := <-w.Error:
				log.Fatalln(err)
			case <-w.Closed:
				return
			}
		}
	}()

	// Watch this folder for changes.
	if err := w.AddRecursive(pathToConfig); err != nil {
		log.Fatalln(err)
	}

	// Start the watching process - it'll check for changes every 100ms.
	if err := w.Start(time.Second); err != nil {
		log.Fatalln(err)
	}
}

/*
func (s *Server) GetConfigChangesBetween(old DeviceConfig, new DeviceConfig) []interface{} {

	if len(old.Services) == len(new.Services) {
		changelog, _ := diff.Diff(old, new, diff.DisableStructValues(), diff.AllowTypeMismatch(true))
		// spew.Dump(changelog)
	}

}
*/
func (s *Server) getBroadcastIp(port int) string {
	return s.BroadcastIP + ":" + fmt.Sprint(port)
}

func (s *Server) GetBroadcastAddress() net.Addr {
	address, err := net.ResolveUDPAddr("udp4", s.getBroadcastIp(s.BroadcastPort))
	if err != nil {
		panic(err)
	}
	return address
}

func (s *Server) GetLocallUDPAddress() *net.UDPAddr {
	address, err := net.ResolveUDPAddr("udp4", ":")
	if err != nil {
		panic(err)
	}
	return address
}

func (s *Server) Broadcast(message Message) int {
	// spew.Dump("message.Recipient.String():")
	// spew.Dump(message.Recipient.String())

	remote, err := net.ResolveUDPAddr("udp4", message.Recipient.String())
	if err != nil {
		log.Fatalln("Error while resolving UDP address. Error: " + err.Error())
		return 0
	}

	conn, err := net.DialUDP("udp4", s.GetLocallUDPAddress(), remote)
	if err != nil {
		log.Fatalln("Error while UDP Dial. Error: " + err.Error())
		return 0
	}
	defer conn.Close()

	message.Hash = message.GenerateHash()

	var nb int
	for i := 0; i < message.GetBurst(); i++ {
		nb, err = conn.Write([]byte(message.String()))
		if err != nil {
			panic(err)
		}
		/* todo: check if the number of bytes is same as original message */
	}

	/* todo: return bool */
	return nb
}

func (s *Server) Listen(port int, OnMessage func(message Message)) {
	pc, err := net.ListenPacket("udp4", ":"+fmt.Sprint(port))
	if err != nil {
		panic(err)
	}
	defer pc.Close()

	/* isUpChannel for network status changes */
	// var networkStatusChange chan bool
	/* Goroutine for checking that status and sending bool over channel */
	/* go func() {
		// TODO: make this so it checks for lan access not internet access!
		for {
			networkStatusChange <- NetworkUp()
			time.Sleep(time.Minute)
		}
	}() */
	/* Goroutine to send "sddp:helloworld" message when networkUp and last was down */
	/* go func() {
		var last bool
		for up := range networkStatusChange {
			if last == false && up == true {
				_ = s.Broadcast(Message{
					Recipient: s.GetBroadcastAddress(),
					Message:   "sddp:helloworld",
				})

				log.Println("<- Broadcasted: sddp:helloworld >> " + s.GetBroadcastAddress().String())
			}

			last = up
		}
	}() */

	/* Create the gorouting  */
	go func() {
		_ = s.Broadcast(Message{
			Recipient: s.GetBroadcastAddress(),
			Message:   "sddp:helloworld",
		})

		log.Println("<- Broadcasted: sddp:helloworld >> " + s.GetBroadcastAddress().String())
		/* Lets wait 10 min before sending the first one */
		time.Sleep(5 * time.Minute)

		for {
			/* Send a heartbeat event every 15 min */
			_ = s.Broadcast(Message{
				Recipient: s.GetBroadcastAddress(),
				Message:   "sddp:heartbeat",
			})
			log.Println("<- Broadcasted: sddp:heartbeat >> " + s.GetBroadcastAddress().String())

			time.Sleep(10 * time.Minute)
		}
	}()

	/* Lets set always send the byebye event when exiting the application */
	/* We capture the interrupt signal and before exiting for real we send the event */
	c := make(chan os.Signal, 1)
	/* Lets get notified when an interrupt happens */
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			/* If there was an interrupt lets ne nice to the network and send a byebye message */
			_ = s.Broadcast(Message{
				Recipient: s.GetBroadcastAddress(),
				Message:   "sddp:byebye",
			})

			log.Println("<- Broadcasted: sddp:byebye >> " + s.GetBroadcastAddress().String())

			/* And now honour the interrupt */
			os.Exit(0)
		}
	}()

	messageMap := MessageHashMap{Messages: []string{}}

	/* Main listen loop */
	for {
		/* Get data from the socket */
		buf := make([]byte, 1024)
		n, rAddress, err := pc.ReadFrom(buf)
		if err != nil {
			panic(err)
		}

		/* Check if this is a message we have send */
		if strings.HasPrefix(rAddress.String(), s.LocalIPAddress.String()) {
			/* lets not bother with stuff we send ourselves */
			continue
		}

		messageString := string(buf[:n])

		/* Create a Messsage struct */
		message, err := NewMessageFromString(rAddress, messageString)
		if messageMap.Contains(message.Hash) {
			continue
		}
		messageMap.Add(message.Hash)

		if err != nil {
			/* Lets see if the message was send in debug mode */
			if !message.Debug {
				/* If not so, lets ignore this mallformed message */
				continue
			}

			/* If the message was send in dev mode we send the debug/error back to the client */
			/* Unicast the error to the client */
			_ = s.Broadcast(Message{
				Recipient: rAddress,
				Message:   "sddp:error:mallformed-data",
				Data: map[string]interface{}{
					"error": err.Error(),
				},
			})

			log.Println("<- Unicasted: sddp:error:mallformed-data \"" + err.Error() + "\" >> " + rAddress.String())
		}

		/* Perform the callback */
		go OnMessage(message)
		/* Handle the internal messages */
		go s.HandleIncomingInternalMessage(message)
	}
}

func (s *Server) HandleIncomingInternalMessage(message Message) {

	switch message.Message {
	case "sddp:discover":
		_ = s.Broadcast(Message{
			Recipient: message.Sender,
			Message:   "sddp:discovered",
			Data:      s.Config.GetBroadcastableDescription(),
		})

		log.Println("<- Unicasted: sddp:discovered >> " + message.Sender.String())
		return

	case "sddp:find":
		return
		// self := s.Config
		_, err := s.Config.Services.Find(message.Data)
		if err != nil {
			return
		}

		_ = s.Broadcast(Message{
			Recipient: message.Sender,
			Message:   "sddp:found",
			Data:      s.Config,
		})
		return
	}
}
