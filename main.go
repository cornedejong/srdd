package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"solrad.nl/sdds/sddp"
)

var mode string
var configPath string

func main() {
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	flag.StringVar(&mode, "mode", "service", "The current mode for the test")
	flag.StringVar(&configPath, "config", homedir+string(os.PathSeparator)+".sdds", "The path to the config dir")

	flag.Parse()

	/* Lets make sure we have atleast some value for config if we're running as a service */
	if mode == "service" && configPath == "" {
		fmt.Fprintf(os.Stderr, "missing required flag: --config\n")
		os.Exit(1)
	}

	switch mode {
	case "service":
		Server()
		break

	case "client":
		Client()
		break

	default:
		log.Fatalln("Mode \"" + mode + "\" is not supported!")
		os.Exit(1)
	}
}

func Server() {
	server := sddp.NewServer(configPath)
	go server.WatchForDeviceChanges(configPath)

	log.Println("Started listening for broadcasts at: " + fmt.Sprint(server.BroadcastIP) + ":" + fmt.Sprint(server.BroadcastPort))
	server.Listen(server.BroadcastPort, func(message sddp.Message) {
		log.Printf("-> %s > %s\n", message.Sender.String(), message.Message)
	})
}

func Client() {
	client := sddp.NewClient()
	client.Broadcast(sddp.Message{
		Recipient: client.GetBroadcastAddress(),
		Message:   "sddp:discover",
	})
	log.Println("<- Broadcasted: sddp:discover >> " + client.GetBroadcastAddress().String())

	/* Hacky, but it works 😅, lets exit this listen mode after 15s */
	// go time.AfterFunc(15*time.Second, func() {
	// 	os.Exit(0)
	// })

	// go ListenForUserInput(client)
	log.Println("Started listening for responses at: " + client.LocalIPAddress.String() + ":" + fmt.Sprint(client.ResponsePort))
	client.Listen(client.ResponsePort, 1*time.Second, func(message sddp.Message) {
		device, _ := PrettyJson(message.Data)
		log.Printf("-> %s > %s\n%s\n", message.Sender.String(), message.Message, device)
	})
}

func PrettyJson(data interface{}) (string, error) {
	buffer := new(bytes.Buffer)
	encoder := json.NewEncoder(buffer)
	encoder.SetIndent("", "    ")

	err := encoder.Encode(data)
	if err != nil {
		return "", err
	}

	return buffer.String(), nil
}
