# SDDS - Simple Device Discovery Service
A very very simple device/service discovery service/protocol

## Protocol (SDDP)
```

sddp - Simple Device Discovery Protocol
a very simple UDP broadcast/unicast device discovery protocol

It consists out of a UDP Broadcast channel for the main broadcasted events.
For the responses to specific requests (like sddp:discover) a separate UDP unicast channel is created

Broadcast channel: UDP socket at XXX.XXX.X.255 port 8166
Response channel:  UDP socket at XXX.XXX.X.XXX port XXXX (can be specified by requesting party/client)

The first three parts of the broadcast channel ip 
are taken from service's own local ip address

The IP address for the response channel is the IP address of the requesting party/client.
The Port for the response channel is up to the client to provide (Default: 8167)

```

### Messages
```

Device Messages:
    sddp:helloworld     // broadcasted when a device connects to the network
    sddp:byebye         // broadcasted when a device leaves the network (probably becomes optional...)
    sddp:heartbeat      // a message send by devices to announce they're still there (default interval ± 10 min)
    sddp:discoverd {}   // a message describing the device that send it 
    sddp:changed        // a message send when the device configuration has changed

Client Messages:
    sddp:discover       // a request for all devices in the network to reply with a discoverd message
    sddp:find {}        // a request for all devices of {"type":"XXX" / "service":"XXX"} to reply with a discoverd message
    sddp:status         // a unicasted message to request the device's status

```


### Usage
```
$ sdds --help
Usage of sdds:
  -config string
    	The path to the device config file (default "$(HOME)/.sdds-profile.json")
  -mode string
    	The current mode for the test (default "service")
```

## Standalone Service

### Usage
```
$ sdds --mode service --config /path/to/.sdds-profile.json
2022/02/10 00:07:54 Started listening for broadcasts at: 192.168.178.255:8166
2022/02/10 00:07:54 <- Broadcasted: sddp:helloworld >> 192.168.178.255:8166
2022/02/10 00:07:59 -> 192.168.178.214:8167 > sddp:discover
2022/02/10 00:07:59 <- Unicasted: sddp:discovered >> 192.168.178.214:8167
...

```



## Client
The client is basically just a showcase. It sends a sddp:discover message and then proceeds to listen for responses.
If you'd want to use it for actual discovery please check the [client lib]()
### Usage
```
$ sdds --mode client
2022/02/10 00:10:40 <- Broadcasted: sddp:discover >> 192.168.178.255:8166
2022/02/10 00:10:40 -> 192.168.178.90:8167 > sddp:discovered {"device":{"type":"nl.solrad.device.webbox:3","serial":60326,"softwareVersion":"3.22.14","Location":"192.168.178.90"},"services":[]}
2022/02/10 00:10:40 -> 192.168.178.69:8167 > sddp:discovered {"device":{"type":"nl.solrad.device.webbox:3","serial":60327,"softwareVersion":"3.22.14","Location":"192.168.178.69"},"services":[]}
```






## todo
X Add support for multiple repeating packets
X Add multi-message spreading support
- Add Discover find message support
- 